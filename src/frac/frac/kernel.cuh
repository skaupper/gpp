#pragma once

#include "mandelbrot_device_types.h"

#include <cuda_runtime.h>
#include <device_launch_parameters.h>


cudaError_t callKernel(const float factor, device_data_t *data, cudaStream_t stream = cudaStreamLegacy);