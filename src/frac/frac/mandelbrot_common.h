#pragma once

#include <complex>

#include "defines.h"
#include "pfc/pfc_types.h"


//
// Types used by the mandelbrot algorithmn
//

using coord_t = float;
using point_t = std::complex<coord_t>;

struct dim_t {
    int width;
    int height;
};

struct boundaries_t {
    point_t topLeft;
    point_t bottomRight;
};


//
// Image specific constants
//

constexpr boundaries_t INITIAL_BOUNDS {
    point_t {-2.74529004f, 1.23807502f},  //
    point_t {1.25470996f, -1.01192498f}   //
};
constexpr point_t INITIAL_DIMS {
    INITIAL_BOUNDS.bottomRight.real() - INITIAL_BOUNDS.topLeft.real(),  //
    INITIAL_BOUNDS.topLeft.imag() - INITIAL_BOUNDS.bottomRight.imag()   //
};

constexpr point_t ZOOM_CENTER {-0.745289981f, 0.113075003f};
constexpr float ZOOM_FACTOR {0.95f};


//
// Constants specific to the process
//

constexpr unsigned int NR_OF_IMAGES {200};


#ifdef SMALL_IMAGES
constexpr unsigned int BITMAP_WIDTH {1024};
constexpr unsigned int BITMAP_HEIGHT {576};
#else
constexpr unsigned int BITMAP_WIDTH {8192};
constexpr unsigned int BITMAP_HEIGHT {4608};
#endif


constexpr size_t PIXELS_PER_IMAGE {BITMAP_HEIGHT * BITMAP_WIDTH};
constexpr size_t MEMORY_PER_IMAGE {sizeof(pfc::pixel_t) * PIXELS_PER_IMAGE};
constexpr size_t TOTAL_MEMORY_CALCULATED {MEMORY_PER_IMAGE * NR_OF_IMAGES};
