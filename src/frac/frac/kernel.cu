#include "kernel.cuh"

#include "mandelbrot_common.h"
#include "mandelbrot_device_constants.h"

#include <cuComplex.h>


//
// Mandelbrot
//

__device__ __forceinline__ static inline float calcNorm(cuComplex z) {
    return cuCrealf(z) * cuCrealf(z) + cuCimagf(z) * cuCimagf(z);
}

__device__ __forceinline__ const pfc::pixel_t calcMandelbrotPoint(const cuComplex &point) {
    constexpr float DIVERGE_THRESHOLD {4.f};
    constexpr pfc::pixel_t CONVERGED_COLOR {0, 0, 0};
    constexpr float COLOR_FACTOR {256.f / DEVICE_MAX_ITERATIONS};

#ifdef DEVICE_UNROLL_INNER_LOOP

    constexpr size_t UNROLLED_ITERATIONS {4};

    size_t diverges {0};
    cuComplex z[UNROLLED_ITERATIONS];
    z[UNROLLED_ITERATIONS - 1] = make_cuFloatComplex(0, 0);

#pragma unroll
    for (size_t i {0}; i < DEVICE_MAX_ITERATIONS; i += UNROLLED_ITERATIONS) {
#pragma unroll
        for (size_t last {UNROLLED_ITERATIONS - 1}, next {0}; next < UNROLLED_ITERATIONS; last = next, next++) {
            z[next] = cuCaddf(cuCmulf(z[last], z[last]), point);
            diverges += size_t {calcNorm(z[next]) >= DIVERGE_THRESHOLD};
        }

        if (diverges != 0) {
            const size_t divergedIndex {(i + UNROLLED_ITERATIONS) - diverges};
            return {0x10, 0x10, (pfc::byte_t)(COLOR_FACTOR * divergedIndex)};
        }
    }

#else

    cuComplex z {point};

#pragma unroll
    for (int i {1}; i < DEVICE_MAX_ITERATIONS; i++) {
        z = cuCaddf(cuCmulf(z, z), point);

        const float norm = calcNorm(z);
        if (norm >= DIVERGE_THRESHOLD) {
            return {0x10, 0x10, (pfc::byte_t)(COLOR_FACTOR * i)};
        }
    }

#endif

    return CONVERGED_COLOR;
}


__global__ static void calcMandlebrotSegment(const coord_t left, const coord_t bottom, const coord_t dx,
                                             const coord_t dy, device_data_t *data) {
    const unsigned int x {blockIdx.x * blockDim.x + threadIdx.x};
    const unsigned int y {blockIdx.y};
    const unsigned int pixelIndex {y * BITMAP_WIDTH + x};

    if (y >= BITMAP_HEIGHT) {
        return;
    }

    const cuComplex p {make_cuFloatComplex(left + dx * x, bottom + dy * y)};
    data[pixelIndex] = calcMandelbrotPoint(p);
}


//
// Kernel caller
//

cudaError_t callKernel(const float factor, device_data_t *data, cudaStream_t stream) {

    const dim3 block_dim {THREADS_IN_BLOCK, 1, 1};
    const dim3 grid_dim {BLOCKS_IN_GRID_X, BLOCKS_IN_GRID_Y, 1};

    const coord_t width {INITIAL_DIMS.real() * factor};
    const coord_t height {INITIAL_DIMS.imag() * factor};

    const coord_t left {ZOOM_CENTER.real() - width / 2};
    const coord_t bottom {ZOOM_CENTER.imag() - height / 2};

    const coord_t dx {width / BITMAP_WIDTH};
    const coord_t dy {height / BITMAP_HEIGHT};

    calcMandlebrotSegment<<<grid_dim, block_dim, 0, stream>>>(left, bottom, dx, dy, data);
    return cudaGetLastError();
}
