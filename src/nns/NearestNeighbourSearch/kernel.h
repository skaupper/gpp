#ifndef KERNEL_H
#define KERNEL_H

#include <vector>

#include <cuda_runtime.h>
#include <device_launch_parameters.h>


typedef float3 Point;
typedef std::vector<Point> PointCollection;
typedef std::vector<size_t> IndexCollection;

IndexCollection calculateNearestNeighboursDevice(const PointCollection& points);

#endif
