
#include "kernel.h"

#include "host_device_functions.h"

#include <iostream>
#include <limits>
#include <stdexcept>




//
// General helper functions
//

static void check(cudaError_t const e) {
    if (e != cudaSuccess) {
        throw std::runtime_error{ cudaGetErrorName(e) };
        //throw std::runtime_error{ cudaGetErrorString(e) };
    }
}




//
// Kernel and kernel caller
//

__global__
static void calculateNearestNeighboursDeviceKernel(size_t* const indices, const Point* const points, const size_t size)
{
    const size_t t = blockIdx.x * blockDim.x + threadIdx.x;
    if (t < size) {
        indices[t] = find_closest(points, &points[t], size);
    }
}

static cudaError_t callKernel(const dim3 big, const dim3 tib, size_t* const dp_dst, const Point * const dp_src, const size_t size)
{
    calculateNearestNeighboursDeviceKernel << < big, tib >> > (dp_dst, dp_src, size);
    return cudaGetLastError();
}



//
// Exported functions
//

IndexCollection calculateNearestNeighboursDevice(const PointCollection& points)
{
    static const int THREADS_IN_BLOCK = 1024;
    static const int BLOCKS_IN_GRID = ((int)points.size() + THREADS_IN_BLOCK - 1) / THREADS_IN_BLOCK;

    const size_t srcSize = points.size() * sizeof(Point);
    const size_t destSize = points.size() * sizeof(size_t);

    // Prepare output collection
    IndexCollection neighbourIndices;
    neighbourIndices.resize(points.size());

    Point* dp_src = nullptr;
    size_t* dp_dst = nullptr;

    try {
        check(cudaSetDevice(0));


        // Allocate device memory
        std::cout << "Allocate device memory..." << std::endl;
        check(cudaMalloc(&dp_src, srcSize));
        check(cudaMalloc(&dp_dst, destSize));


        // Copy data to device
        std::cout << "Copy points to device..." << std::endl;
        check(cudaMemcpy(dp_src, std::data(points), srcSize, cudaMemcpyHostToDevice));

        // Calculate nearest neighbours and time it!
        std::cout << "Calculate distances (on device, " << THREADS_IN_BLOCK << " threads/block) ..." << std::endl;
        check(callKernel(BLOCKS_IN_GRID, THREADS_IN_BLOCK, dp_dst, dp_src, points.size()));
        check(cudaDeviceSynchronize());

        // Copy back results
        std::cout << "Copy indices from device..." << std::endl;
        check(cudaMemcpy(std::data(neighbourIndices), dp_dst, destSize, cudaMemcpyDeviceToHost));


        // Free device memory
        std::cout << "Free device memory..." << std::endl;

        check(cudaFree(dp_dst));
        dp_dst = nullptr;

        check(cudaFree(dp_src));
        dp_src = nullptr;

        // Reset device
        check(cudaDeviceReset());
    }
    catch (const std::exception& ex) {
        // Free device memory
        if (dp_dst) {
            cudaFree(dp_dst);
        }
        if (dp_src) {
            cudaFree(dp_src);
        }

        std::cerr << "Error '" << ex.what() << "'" << std::endl;
        return {};
    }

    return neighbourIndices;
}
