#pragma once

constexpr unsigned int HOST_MAX_ITERATIONS {128};
constexpr unsigned int HOST_NR_OF_PARALLEL_IMAGES {50};
constexpr unsigned int HOST_NR_OF_TASKS_PER_IMAGE {100};