#include "mandelbrot_device.h"

#include "kernel.cuh"
#include "pfc/pfc_threading.h"


using namespace pfc;


static constexpr void check(cudaError_t const e) {
    if (e != cudaSuccess) {
        std::cerr << cudaGetErrorString(e) << std::endl;
        throw std::runtime_error {cudaGetErrorString(e)};
    }
}


PreparedDeviceData prepareData_Device() {
    PreparedDeviceData data;

    check(cudaDeviceReset());
    check(cudaSetDevice(0));

    check(cudaMalloc(&data.devData, DEVICE_NR_OF_PARALLEL_IMAGES * MEMORY_PER_IMAGE));
    check(cudaMallocHost(&data.hostData, DEVICE_NR_OF_PARALLEL_IMAGES * MEMORY_PER_IMAGE));

    std::for_each(std::begin(data.bitmaps), std::end(data.bitmaps),
                  [](auto &bmp) { bmp.create(BITMAP_WIDTH, BITMAP_HEIGHT); });

    std::for_each(std::begin(data.streams), std::end(data.streams),
                  [](auto &stream) { check(cudaStreamCreate(&stream)); });

    return data;
}

void cleanupData_Device(PreparedDeviceData &data) {

    check(cudaFree(data.devData));
    check(cudaFreeHost(data.hostData));
    data.devData = nullptr;
    data.hostData = nullptr;

    std::for_each(std::begin(data.bitmaps), std::end(data.bitmaps), [](auto &bmp) { bmp.clear(); });

    std::for_each(std::begin(data.streams), std::end(data.streams),
                  [](auto &stream) { check(cudaStreamDestroy(stream)); });

    check(cudaDeviceReset());
}


void calcMandelbrotZoomRide_Device(const std::string &filePathPrefix, PreparedDeviceData &data) {

    parallel_range(DEVICE_NR_OF_PARALLEL_IMAGES, NR_OF_IMAGES, [&](auto tid, auto begin, auto end) {
        auto *localDevData {data.devData + PIXELS_PER_IMAGE * tid};
        auto *localHostData {data.hostData + PIXELS_PER_IMAGE * tid};
        float zoom {(float)std::pow(ZOOM_FACTOR, begin)};

        for (auto image {begin}; image < end; ++image) {
            check(callKernel(zoom, localDevData, data.streams[tid]));
            check(cudaStreamSynchronize(data.streams[tid]));

            check(cudaMemcpy(localHostData, localDevData, MEMORY_PER_IMAGE, cudaMemcpyDeviceToHost));

#ifdef WRITE_DEVICE_BITMAPS
            std::memcpy(std::data(data.bitmaps[tid]), localHostData, MEMORY_PER_IMAGE);
            const auto fileName {filePathPrefix + "-device-" + std::to_string(image) + ".bmp"};
            data.bitmaps[tid].to_file(fileName);
#endif

            zoom *= ZOOM_FACTOR;
        }
    });
}
