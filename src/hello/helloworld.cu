
#include "cuda_runtime.h"
#include "device_launch_parameters.h"

#include <iostream>
#include <string>



//
// Kernel function
//

__global__ void copyStringKernel(char* dst, const char* const src, int len)
{
    int idx = threadIdx.x;

    if (idx < len) {
        dst[idx] = src[idx];
    }
}



//
// Host functions
//

template<size_t N>
constexpr size_t length(char const (&)[N])
{
    return N - 1;
}

__host__ void printCudaDevices()
{
    int deviceCount;
    cudaError_t err;

    err = cudaGetDeviceCount(&deviceCount);
    if (err != cudaSuccess) {
        std::cerr << "Failed to query CUDA device count: " << cudaGetErrorString(err) << std::endl;
        return;
    }

    cudaDeviceProp prop;
    for (int dev = 0; dev < deviceCount; ++dev) {
        err = cudaGetDeviceProperties(&prop, dev);
        if (err != cudaSuccess) {
            std::cerr << "Failed to query properties of CUDA device #" << dev << ": " << cudaGetErrorString(err) << std::endl;
            break;
        }

        std::cout << "device #" << dev << "          : " << prop.name << std::endl;
        std::cout << "compute capability : " << prop.major << "." << prop.minor << std::endl;
        std::cout << "architecture       : ?" << std::endl;
    }
}

__host__ cudaError_t copyString()
{
    // Host variables
    const char hp_src[] = "hello_world";
    char hp_dst[length(hp_src)+1] = { 0 };
    int hp_src_len = static_cast<int>((length(hp_src)+1) * sizeof(char));

    cudaError_t cudaStatus;

    // Device variables
    char* dev_hp_src;
    char* dev_hp_dst;



    // Choose which GPU to run on, change this on a multi-GPU system.
    cudaStatus = cudaSetDevice(0);
    if (cudaStatus != cudaSuccess) {
        std::cerr << "Failed to set CUDA device: " << cudaGetErrorString(cudaStatus) << std::endl;
        goto Error_beforemallocs;
    }


    // Allocate GPU buffers for input and output string
    cudaStatus = cudaMalloc((void**)&dev_hp_src, hp_src_len);
    if (cudaStatus != cudaSuccess) {
        std::cerr << "Failed to allocated device memory for source string: " << cudaGetErrorString(cudaStatus) << std::endl;
        goto Error_beforemallocs;
    }
    cudaStatus = cudaMalloc((void**)&dev_hp_dst, hp_src_len);
    if (cudaStatus != cudaSuccess) {
        std::cerr << "Failed to allocated device memory for destination string: " << cudaGetErrorString(cudaStatus) << std::endl;
        goto Error_dst;
    }


    // print strings before kernel invocation
    std::cout << "hp_src: '" << hp_src << "'" << std::endl;
    std::cout << "hp_dst: '" << hp_dst << "'" << std::endl;
    std::cout << std::endl;


    // Copy input string to device
    cudaStatus = cudaMemcpy(dev_hp_src, hp_src, hp_src_len, cudaMemcpyHostToDevice);
    if (cudaStatus != cudaSuccess) {
        std::cerr << "Failed to copy source string to device: " << cudaGetErrorString(cudaStatus) << std::endl;
        goto Error_aftermallocs;
    }
    
    // Launch a kernel on the GPU with one thread for each character
    copyStringKernel << <1, hp_src_len >> > (dev_hp_dst, dev_hp_src, hp_src_len);

    // Check for any errors launching the kernel
    cudaStatus = cudaGetLastError();
    if (cudaStatus != cudaSuccess) {
        std::cerr << "Failed to launch kernel function: " << cudaGetErrorString(cudaStatus) << std::endl;
        goto Error_aftermallocs;
    }

    // cudaDeviceSynchronize waits for the kernel to finish, and returns
    // any errors encountered during the launch.
    cudaStatus = cudaDeviceSynchronize();
    if (cudaStatus != cudaSuccess) {
        std::cerr << "Failed to synchronize kernel: " << cudaGetErrorString(cudaStatus) << std::endl;
        goto Error_aftermallocs;
    }
       
    // Copy resulting string from device
    cudaStatus = cudaMemcpy(hp_dst, dev_hp_dst, hp_src_len, cudaMemcpyDeviceToHost);
    if (cudaStatus != cudaSuccess) {
        std::cerr << "Failed to copy resulting string from device: " << cudaGetErrorString(cudaStatus) << std::endl;
        goto Error_aftermallocs;
    }


    // print strings before kernel invocation
    std::cout << "hp_src: '" << hp_src << "'" << std::endl;
    std::cout << "hp_dst: '" << hp_dst << "'" << std::endl;
    std::cout << std::endl;


Error_aftermallocs:
    cudaFree(dev_hp_dst);
Error_dst:
    cudaFree(dev_hp_src);
Error_beforemallocs:
    return cudaStatus;
}



//
// Entrypoint
//

int main()
{
    printCudaDevices();
    std::cout << std::endl;
    copyString();
    return 0;
}