#pragma once

#include <string>


struct GPUInfo {
    std::string name;
    std::string computeCapability;
};


std::string getCPUBrandName();
GPUInfo getGPUInfo(const int deviceNr = 0);

size_t getAsMib(const size_t bytes);