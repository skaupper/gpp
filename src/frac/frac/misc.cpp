#include "misc.h"

#include <array>
#include <cmath>
#include <intrin.h>
#include <stdexcept>

#include <cuda_runtime.h>


static void trimString(std::string &str) {
    // https://stackoverflow.com/questions/216823/whats-the-best-way-to-trim-stdstring
    size_t endpos   = str.find_last_not_of(" \t");
    size_t startpos = str.find_first_not_of(" \t");
    if (std::string::npos != endpos) {
        str = str.substr(0, endpos + 1);
        str = str.substr(startpos);
    } else {
        str.erase(std::remove(std::begin(str), std::end(str), ' '), std::end(str));
    }
}


std::string getCPUBrandName() {
    constexpr unsigned int MAX_EXTENDED_FUNCTIONS_INPUT_OFFSET {0x80000000};
    constexpr unsigned int BRAND_NAME_OFFSET {0x80000002};

    std::array<int, 4> cpuInfo;

    __cpuid(std::data(cpuInfo), MAX_EXTENDED_FUNCTIONS_INPUT_OFFSET);
    const auto extIds {cpuInfo[0]};
    if (extIds - MAX_EXTENDED_FUNCTIONS_INPUT_OFFSET < 4) {
        throw std::runtime_error("Could not retrieve CPU information!");
    }

    // The CPU brand string is returned in 3 chunks of 16 bytes, so the string is always 40 bytes in size.
    std::string cpuBrand(0x40, '\0');

    for (int i {0}; i < 3; ++i) {
        __cpuid(std::data(cpuInfo), BRAND_NAME_OFFSET + i);
        std::memcpy(std::data(cpuBrand) + size_t {0x10} * i, std::data(cpuInfo),
                    std::size(cpuInfo) * sizeof(decltype(cpuInfo)::value_type));
    }

    trimString(cpuBrand);
    return cpuBrand;
}

GPUInfo getGPUInfo(const int deviceNr) {
    GPUInfo gpuInfo;

    cudaDeviceProp properties;
    const auto status = cudaGetDeviceProperties(&properties, deviceNr);
    if (status != cudaSuccess) {
        throw std::runtime_error("Could not retrieve GPU information!");
    }

    gpuInfo.name              = properties.name;
    gpuInfo.computeCapability = std::to_string(properties.major) + "." + std::to_string(properties.minor);

    return gpuInfo;
}


size_t getAsMib(const size_t bytes) {
    return static_cast<size_t>(bytes / std::pow(2, 20));
}