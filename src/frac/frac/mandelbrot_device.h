#pragma once

#include "mandelbrot_common.h"
#include "mandelbrot_device_constants.h"
#include "mandelbrot_device_types.h"

#include "pfc/pfc_bitmap_3.h"

#include <array>

#include <cuda_runtime.h>


//
// Helper types
//

struct PreparedDeviceData {
    std::array<pfc::bitmap, DEVICE_NR_OF_PARALLEL_IMAGES> bitmaps;
    std::array<cudaStream_t, DEVICE_NR_OF_PARALLEL_IMAGES> streams;
    device_data_t *devData;
    device_data_t *hostData;
};


//
// Function prototypes
//

PreparedDeviceData prepareData_Device();
void cleanupData_Device(PreparedDeviceData &data);
void calcMandelbrotZoomRide_Device(const std::string &filePathPrefix, PreparedDeviceData &data);