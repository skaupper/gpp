#pragma once

#include "kernel.h"

//
// NNS implementation
//

__host__ __device__ __forceinline__
float normSquared(const float3 p1, const float3 p2)
{
    float dx, dy, dz;
    dx = p1.x - p2.x;
    dy = p1.y - p2.y;
    dz = p1.z - p2.z;
    return dx * dx + dy * dy + dz * dz;
}


__host__ __device__ __forceinline__
int find_closest(const float3* p_points, const float3* const p_point, const size_t length) {
    int index = -1;
    auto min_so_far = std::numeric_limits<float>::max();

    for (auto to = 0; to < length; ++p_points, ++to) {
        if (p_points != p_point) {
            auto const dist = normSquared(*p_point, *p_points);

            if (dist < min_so_far) {
                min_so_far = dist; index = to;
            }
        }
    }

    return index;
}