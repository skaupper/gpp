#pragma once

#include "mandelbrot_common.h"
#include "mandelbrot_host_constants.h"

#include "pfc/pfc_bitmap_3.h"


//
// Helper types
//

using host_bitmap_arr_t = std::array<pfc::bitmap, HOST_NR_OF_PARALLEL_IMAGES>;

struct PreparedHostData {
    host_bitmap_arr_t bitmaps;
};


//
// Function prototypes
//

PreparedHostData prepareData_Host();
void cleanupData_Host(PreparedHostData &data);
void calcMandelbrotZoomRide_Host(const std::string &filePathPrefix, PreparedHostData &data);
