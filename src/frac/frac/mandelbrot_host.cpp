#include "mandelbrot_host.h"

#include "pfc/pfc_threading.h"

using namespace pfc;


//
// Color LUT
//

static std::array<pfc::pixel_t, HOST_MAX_ITERATIONS> colorLut;

static constexpr void precalcColorMap() {
    const float COLOR_FACTOR {255.f / HOST_MAX_ITERATIONS};

    for (size_t i {0}; i < HOST_MAX_ITERATIONS; ++i) {
        const pfc::byte_t red {(pfc::byte_t)(i * COLOR_FACTOR)};
        colorLut[i] = {0x10, 0x10, red};
    }
}

static constexpr pfc::pixel_t getColor(size_t index) {
    return colorLut[index];
}


//
// Helpers
//

static point_t &square(point_t &p) {
    // Avoid generation of intermediate objects -> calculate the square in-place
    const auto imag {p.real() * p.imag() * 2};
    p.real(p.real() * p.real() - p.imag() * p.imag());
    p.imag(imag);
    return p;
}


//
// Mandelbrot
//

static constexpr BGR_4_t calcMandelbrotPoint(const point_t &point) {
    constexpr size_t DIVERGE_THRESHOLD {4};
    constexpr pixel_t CONVERGED_COLOR {0, 0, 0};

    point_t z {point};

    for (size_t i {1}; i < HOST_MAX_ITERATIONS; ++i) {
        square(z) += point;

        if (std::norm(z) >= DIVERGE_THRESHOLD) {
            return getColor(i);
        }
    }

    return CONVERGED_COLOR;
}

static void calcMandelbrotImage(const boundaries_t &bounds, bitmap &bmp) {
    // dx and dy represent the change per pixel
    const coord_t dx {(bounds.bottomRight.real() - bounds.topLeft.real()) / bmp.width()};
    const coord_t dy {(bounds.topLeft.imag() - bounds.bottomRight.imag()) / bmp.height()};

    // extract image buffer
    auto &span {bmp.pixel_span()};
    const auto bufferSize {std::size(span)};


    parallel_range<int>(HOST_NR_OF_TASKS_PER_IMAGE, bmp.height(), [&](auto _, auto begin, auto end) {
        const auto width {bmp.width()};
        const auto index {begin * width};
        auto *pixelBuffer {std::data(span) + index};

        // The starting point for this section of the image
        point_t p {coord_t {bounds.topLeft.real()},  //
                   coord_t {bounds.bottomRight.imag() + dy * begin}};


        for (auto y {begin}; y < end; ++y) {
            for (size_t x {0}; x < width; ++x, ++pixelBuffer) {
                // For each pixel calculate the corresponding color and recalculate the next point
                *pixelBuffer = calcMandelbrotPoint(p);
                p.real(p.real() + dx);
            }

            p.real(bounds.topLeft.real());
            p.imag(p.imag() + dy);
        }
    });
}


//
// Exported functions
//

void calcMandelbrotZoomRide_Host(const std::string &filePathPrefix, PreparedHostData &data) {
    host_bitmap_arr_t &bmps {data.bitmaps};

    // Split the zoom ride into equally sized sets of images
    parallel_range(HOST_NR_OF_PARALLEL_IMAGES, NR_OF_IMAGES, [&](auto tid, auto begin, auto end) {
        // Compute the starting values for the current image
        // std::pow is not the most efficient, but it is only executed once per image set...
        point_t currentDims {INITIAL_DIMS * (float)std::pow(ZOOM_FACTOR, begin)};
        boundaries_t currentBounds;

        for (auto i = begin; i < end; ++i) {
            // Recalculate bounds and dimensions for the next image
            currentBounds = {
                point_t {ZOOM_CENTER.real() - currentDims.real() / 2, ZOOM_CENTER.imag() + currentDims.imag() / 2},  //
                point_t {ZOOM_CENTER.real() + currentDims.real() / 2, ZOOM_CENTER.imag() - currentDims.imag() / 2}   //
            };
            currentDims *= ZOOM_FACTOR;

            // Calculate next image
            calcMandelbrotImage(currentBounds, bmps[tid]);

#ifdef WRITE_HOST_BITMAPS
            const auto fileName {filePathPrefix + "-host-" + std::to_string(i) + ".bmp"};
            bmps[tid].to_file(fileName);
#endif
        }
    });
}


PreparedHostData prepareData_Host() {
    PreparedHostData data;

    std::for_each(std::begin(data.bitmaps), std::end(data.bitmaps),
                  [](auto &&bmp) { bmp.create(BITMAP_WIDTH, BITMAP_HEIGHT); });

    precalcColorMap();

    return data;
}

void cleanupData_Host(PreparedHostData &data) {
    std::for_each(std::begin(data.bitmaps), std::end(data.bitmaps), [](auto &bmp) { bmp.clear(); });
}