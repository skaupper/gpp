//       $Id: HelloWorldKernel.cuh 39790 2019-12-05 09:05:49Z p20068 $
//      $URL: https://svn01.fh-hagenberg.at/bin/cepheiden/vocational/teaching/SE-Master/MPV3/2019-WS/ILV/src/HelloWorld/src/HelloWorldKernel.cuh $
// $Revision: 39790 $
//     $Date: 2019-12-05 10:05:49 +0100 (Do., 05 Dez 2019) $
//   Creator: Peter Kulczycki
//  Creation: October, 2019
//   $Author: p20068 $
// Copyright: (c) 2019 Peter Kulczycki (peter.kulczycki<AT>fh-hagenberg.at)
//   License: This document contains proprietary information belonging to
//            University of Applied Sciences Upper Austria, Campus Hagenberg. It
//            is distributed under the Boost Software License, Version 1.0 (see
//            http://www.boost.org/LICENSE_1_0.txt).

#pragma once

#include <cuda_runtime.h>
#include <device_launch_parameters.h>

#include <cstddef>

cudaError_t call_cs_kernel (dim3 big, dim3 tib, char * dp_dst, char const * dp_src, std::size_t size);
