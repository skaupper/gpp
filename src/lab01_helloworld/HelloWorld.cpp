//       $Id: HelloWorld.cpp 40466 2020-03-31 10:20:22Z p20068 $
//      $URL: https://svn01.fh-hagenberg.at/bin/cepheiden/vocational/teaching/SE-Master/MPV3/2019-WS/ILV/src/HelloWorld/src/HelloWorld.cpp $
// $Revision: 40466 $
//     $Date: 2020-03-31 12:20:22 +0200 (Di., 31 Mär 2020) $
//   Creator: Peter Kulczycki
//  Creation: October, 2019
//   $Author: p20068 $
// Copyright: (c) 2019 Peter Kulczycki (peter.kulczycki<AT>fh-hagenberg.at)
//   License: This document contains proprietary information belonging to
//            University of Applied Sciences Upper Austria, Campus Hagenberg. It
//            is distributed under the Boost Software License, Version 1.0 (see
//            http://www.boost.org/LICENSE_1_0.txt).

#include "./HelloWorldKernel.cuh"

#include <iostream>
#include <memory>
#include <stdexcept>
#include <string_view>

using namespace std::string_view_literals;

void check (cudaError_t const e) {
   if (e != cudaSuccess) {
      throw std::runtime_error {cudaGetErrorName (e)};
   }
}

void checked_main () {
   int count {0}; check (cudaGetDeviceCount (&count));

   if (0 < count) {
      check (cudaSetDevice (0));

      cudaDeviceProp prop {}; check (cudaGetDeviceProperties (&prop, 0));

      std::cout << prop.name << '\n';
      std::cout << prop.major << '.' << prop.minor << '\n';

      auto const src    {"Lorem ipsum dolor sit amet, consetetur sadipscing elitr ..."sv};
      auto const size   {static_cast <int> (std::size (src)) + 1};
      auto const hp_dst {std::make_unique <char []> (size)};

      char * dp_src {nullptr}; check (cudaMalloc (&dp_src, size));
      char * dp_dst {nullptr}; check (cudaMalloc (&dp_dst, size));

      check (cudaMemcpy (dp_src, std::data (src), size, cudaMemcpyHostToDevice));

      auto const tib {32};
      check (call_cs_kernel ((size + tib - 1) / tib, tib, dp_dst, dp_src, size));

      check (cudaMemcpy (hp_dst.get (), dp_dst, size, cudaMemcpyDeviceToHost));

      check (cudaFree (dp_dst));
      check (cudaFree (dp_src));

      std::cout << "'" << hp_dst << "'\n";
   }

   check (cudaDeviceReset ());
}

int main () {
   try {
      checked_main ();
   } catch (std::exception const & x) {
      std::cerr << "Error '" << x.what () << "'\n";
   }
}
