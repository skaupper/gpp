#include "kernel.h"
#include "host_device_functions.h"

#include "pfc_random.h"
#include "pfc_threading.h"
#include "pfc_timing.h"

#include <chrono>
#include <cassert>
#include <array>
#include <random>
#include <iostream>
#include <iomanip>
#include <thread>


#define NOMINMAX
#define WIN32_LEAN_AND_MEAN
#include <Windows.h> // Used for SetPriorityClass(...) 


using namespace std::chrono_literals;


constexpr size_t NR_OF_POINTS = 50000;
constexpr float COORD_MIN_VALUE = -100e3;
constexpr float COORD_MAX_VALUE = 100e3;

static const size_t CONCURRENCY = size_t{ pfc::hardware_concurrency() }*2;



//
// General helper functions
// 

void compareIndices(const IndexCollection& hostIndices, const IndexCollection& deviceIndices)
{
    constexpr size_t NR_OF_INDICES_TO_PRINT = 20;
    constexpr int LOOP_INDEX_WIDTH = 2;
    constexpr int POINT_INDEX_WIDTH = 6;

    assert(hostIndices.size() == deviceIndices.size());
    assert(hostIndices == deviceIndices);
    for (int i = 0; i < std::min(hostIndices.size(), NR_OF_INDICES_TO_PRINT); ++i) {
        std::cout << std::setw(LOOP_INDEX_WIDTH) << i << " --- ";
        std::cout << std::setw(POINT_INDEX_WIDTH) << hostIndices[i] << " --- ";
        std::cout << std::setw(POINT_INDEX_WIDTH) << deviceIndices[i];

        if (hostIndices[i] != deviceIndices[i]) {
            std::cout << "    !!!";
        }
        std::cout << std::endl;
    }
}

void printPoint(const Point p)
{
    constexpr int MAX_FLOAT_SIZE = 10;
    std::cout << "Point {" 
        << std::setiosflags(std::ios_base::fixed)
        << std::setprecision(2)
        << std::setfill(' ')
        << "x: " << std::setw(MAX_FLOAT_SIZE) << p.x << "; "
        << "y: " << std::setw(MAX_FLOAT_SIZE) << p.y << "; "
        << "z: " << std::setw(MAX_FLOAT_SIZE) << p.z 
        << "}" << std::endl;
}

Point generatePoint()
{
    return Point{
        .x = pfc::get_random_uniform(COORD_MIN_VALUE, COORD_MAX_VALUE),
        .y = pfc::get_random_uniform(COORD_MIN_VALUE, COORD_MAX_VALUE),
        .z = pfc::get_random_uniform(COORD_MIN_VALUE, COORD_MAX_VALUE)
    };
}



//
// NNS host-side calculations
//

void calculateNearestNeighboursHostThread(const PointCollection& points, IndexCollection& indices, const size_t startIndex, const size_t endIndex)
{
    for (auto i = startIndex; i < endIndex; ++i) {
        indices[i] = find_closest(points.data(), &points[i], points.size());
    }
}

IndexCollection calculateNearestNeighboursHost(const PointCollection& points)
{
    // Each thread will get a block of points to process.
    // If the number of points is not a multiple of HW_CONCURRENCY this algorithm does not work at maximum efficiency!
    const int INDEX_INCREMENT = (int)std::ceil((float)points.size() / CONCURRENCY);

    // Prepare output collection
    IndexCollection neighbourIndices;
    neighbourIndices.resize(NR_OF_POINTS);

    // Start a reasonable amount of tasks to calculate the nearest neighbour for each point
    pfc::parallel_range(CONCURRENCY, points.size(), [&points, &neighbourIndices](auto tid, auto begin, auto end) {
        calculateNearestNeighboursHostThread(points, neighbourIndices, begin, end);
    });    

    return neighbourIndices;
}



//
// Entrypoint
//

int main()
{
    std::cout << "Set process priority to realtime..." << std::endl;
    SetPriorityClass(GetCurrentProcess(), REALTIME_PRIORITY_CLASS);


    PointCollection points;

    std::cout << "Allocate host memory..." << std::endl;
    points.resize(NR_OF_POINTS);

    std::cout << "Generate " << NR_OF_POINTS << " points..." << std::endl;
    std::generate_n(points.begin(), NR_OF_POINTS, generatePoint);

    std::cout << std::endl;



    //
    // Device side execution
    //

    std::cout << "Initiate device execution ..." << std::endl;

    IndexCollection indicesDevice;
    auto elapsedDevice = pfc::timed_run([&points, &indicesDevice]() {
        indicesDevice = calculateNearestNeighboursDevice(points);
    });


    std::cout << std::endl;


    //
    // Host side execution
    //

    std::cout << "Warm up CPU ..." << std::endl;
    pfc::warm_up_cpu(5s);

    std::cout << "Calculate distances (on host, " << CONCURRENCY << " threads) ..." << std::endl;

    IndexCollection indicesHost;
    auto elapsedHost = pfc::timed_run([&points, &indicesHost]() {
        indicesHost = calculateNearestNeighboursHost(points);
    });


    std::cout << std::endl;
    std::cout << std::endl;


    //
    // Compare host and device results
    //

    compareIndices(indicesHost, indicesDevice);


    std::cout << std::endl;
    std::cout << std::endl;


    //
    // Speed comparison
    //

    std::cout << "Device side calculation took " << std::chrono::duration_cast<std::chrono::milliseconds>(elapsedDevice) << std::endl;
    std::cout << "Host side calculation took " << std::chrono::duration_cast<std::chrono::milliseconds>(elapsedHost) << std::endl;
    std::cout << std::endl;

    const int speedup = static_cast<int>((float)elapsedHost.count() / elapsedDevice.count() * 100);
    std::cout << "Device speedup: " << speedup << "%" << std::endl;


    return 0;
}