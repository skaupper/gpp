#pragma once

#include "mandelbrot_common.h"

constexpr unsigned int DEVICE_NR_OF_PARALLEL_IMAGES {4};
constexpr unsigned int DEVICE_MAX_ITERATIONS {128};


constexpr int THREADS_IN_BLOCK {256};
constexpr int BLOCKS_IN_GRID_X {BITMAP_WIDTH / THREADS_IN_BLOCK};
constexpr int BLOCKS_IN_GRID_Y {BITMAP_HEIGHT};


static_assert(BITMAP_WIDTH % THREADS_IN_BLOCK == 0,
              "The bitmap width has to be a integer multiple of the amount of threads in one block.");