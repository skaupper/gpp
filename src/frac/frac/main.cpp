#include "mandelbrot_device.h"
#include "mandelbrot_host.h"
#include "misc.h"

#include "pfc/pfc_timing.h"

#include <filesystem>
#include <iostream>


int main() {
    // Create output directory
    std::filesystem::path outputPath {std::filesystem::current_path().append("output")};
    std::filesystem::create_directory(outputPath);

    const std::string filePathPrefix {outputPath.append("mandelbrot").string()};

    // ---------------------------------------------------------------------------------------------------------

    std::cout << NR_OF_IMAGES << " images with " << BITMAP_WIDTH << "x" << BITMAP_HEIGHT << " pixels ("
              << getAsMib(TOTAL_MEMORY_CALCULATED) << " MiB)" << std::endl;
    std::cout << std::endl;


    // ---------------------------------------------------------------------------------------------------------

    //
    // Prepare CPU
    //

    /*
    std::cout << "Set process priority to realtime ... " << std::endl;
    SetPriorityClass(GetCurrentProcess(), REALTIME_PRIORITY_CLASS);
    std::cout << "done" << std::endl;
    */

#ifndef DISABLE_HOST
    pfc::warm_up_cpu();
    PreparedHostData hostData = prepareData_Host();
#endif


    //
    // Prepare device
    //

#ifndef DISABLE_DEVICE
    PreparedDeviceData deviceData;
    try {
        deviceData = prepareData_Device();
    } catch (const std::runtime_error &ex) {
        std::cerr << "Failed to prepare device data: " << ex.what() << std::endl;
        exit(1);
    }
#endif


    // ---------------------------------------------------------------------------------------------------------

    //
    // Host side calculations
    //

    std::cout << "CPU            : " << getCPUBrandName() << std::endl;

#ifndef DISABLE_HOST
    const auto elapsedHost {pfc::timed_run([&] { calcMandelbrotZoomRide_Host(filePathPrefix, hostData); })};
    const auto elapsedHostMs {std::chrono::duration_cast<std::chrono::milliseconds>(elapsedHost)};

    std::cout << "Execution time : " << elapsedHostMs << std::endl;
    std::cout << "Throughput     : " << getAsMib(TOTAL_MEMORY_CALCULATED) / (elapsedHostMs.count() / 1000.) << " MiB/s"
              << std::endl;
#else
    std::cout << "!!! Disabled !!!" << std::endl;
#endif

    std::cout << std::endl;


    //
    // Device side calculations
    //

    auto [name, cc] = getGPUInfo();
    std::cout << "GPU            : " << name << std::endl;
    std::cout << "CC             : " << cc << std::endl;

#ifndef DISABLE_DEVICE
    const auto elapsedDev {pfc::timed_run([&] {
        try {
            calcMandelbrotZoomRide_Device(filePathPrefix, deviceData);
        } catch (const std::runtime_error &ex) {
            std::cout << "Failed to run device code: " << ex.what() << std::endl;
            exit(1);
        }
    })};

    const auto elapsedDevMs {std::chrono::duration_cast<std::chrono::milliseconds>(elapsedDev)};

    std::cout << "Execution time : " << elapsedDevMs << std::endl;
    std::cout << "Throughput     : " << getAsMib(TOTAL_MEMORY_CALCULATED) / (elapsedDevMs.count() / 1000.) << " MiB/s"
              << std::endl;
#else
    std::cout << "!!! Disabled !!!" << std::endl;
#endif

    std::cout << std::endl;


    // ---------------------------------------------------------------------------------------------------------

    //
    // Cleanup
    //

#ifndef DISABLE_HOST
    cleanupData_Host(hostData);
#endif

#ifndef DISABLE_DEVICE
    cleanupData_Device(deviceData);
#endif


    // ---------------------------------------------------------------------------------------------------------

    //
    // Calc speedup
    //
#ifndef DISABLE_DEVICE
#ifndef DISABLE_HOST
    const double speedup {elapsedHostMs.count() * 1. / elapsedDevMs.count()};
    std::cout << "Speedup: " << speedup * 100 << "%" << std::endl;
#endif
#endif

    return 0;
}