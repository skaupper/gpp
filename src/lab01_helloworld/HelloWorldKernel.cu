//       $Id: HelloWorldKernel.cu 39790 2019-12-05 09:05:49Z p20068 $
//      $URL: https://svn01.fh-hagenberg.at/bin/cepheiden/vocational/teaching/SE-Master/MPV3/2019-WS/ILV/src/HelloWorld/src/HelloWorldKernel.cu $
// $Revision: 39790 $
//     $Date: 2019-12-05 10:05:49 +0100 (Do., 05 Dez 2019) $
//   Creator: Peter Kulczycki
//  Creation: October, 2019
//   $Author: p20068 $
// Copyright: (c) 2019 Peter Kulczycki (peter.kulczycki<AT>fh-hagenberg.at)
//   License: This document contains proprietary information belonging to
//            University of Applied Sciences Upper Austria, Campus Hagenberg. It
//            is distributed under the Boost Software License, Version 1.0 (see
//            http://www.boost.org/LICENSE_1_0.txt).

#include "./HelloWorldKernel.cuh"

template <typename char_t, typename size_t> __global__ void cs_kernel (char_t * const dp_dst, char_t const * const dp_src, size_t const size) {
   size_t const t {blockIdx.x * blockDim.x + threadIdx.x};

   if (t < size) {
      dp_dst[t] = dp_src[t];
   }
}

cudaError_t call_cs_kernel (dim3 const big, dim3 const tib, char * const dp_dst, char const * const dp_src, std::size_t const size) {
    cs_kernel << <big, tib >> > (dp_dst, dp_src, size); return cudaGetLastError();
}
